<?php

use Illuminate\Database\Seeder;

class MoviesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('movies')->insert([[
            'title' => 'Call me by your name',
            'year' => 1939,
            'artist_id' => 2,
        ], [
            'title' => 'Climax',
            'year' => 2019,
            'artist_id' => 2,
        ]]);
    }
}
