<?php

use Illuminate\Database\Seeder;

class RoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rooms')->insert([[
            'name' => 'Salle Rex',
            'capacity' => 150
        ], [
            'name' => 'Salle T-Spoon',
            'capacity' => 50
        ], [
            'name' => 'Salle Gstar',
            'capacity' => 250
        ], [
            'name' => 'Salle Ouest',
            'capacity' => 275
        ]]);
    }
}
