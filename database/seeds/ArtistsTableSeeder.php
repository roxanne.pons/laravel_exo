<?php

use Illuminate\Database\Seeder;

class ArtistsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('artists')->insert([[
            'name' => 'Francis Ford',
            'firstname' => 'Coppola',
            'birthdate' => 1939,
        ], [
            'name' => 'Lynch',
            'firstname' => 'David',
            'birthdate' => 1946,
        ]]);
    }
}
