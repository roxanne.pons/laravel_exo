<?php

use Illuminate\Database\Seeder;

class CinemasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cinemas')->insert([[
            'name' => 'Cinema fun',
            'street' => 'Rue du chocolat 15',
            'postcode' => 1009,
            'city' => 'Madrid',
            'country' => 'Spain'
        ], [
            'name' => 'MaxiCiné',
            'street' => 'Chemin de la mort 12',
            'postcode' => 2039,
            'city' => 'Bruxelles',
            'country' => 'Belgium'
        ]]);
    }
}
