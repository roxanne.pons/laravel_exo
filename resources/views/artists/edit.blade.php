@extends('layouts.app')

@section('title', 'Artist Name')

@section('content')
<div class="container" style="margin-top: 10em; width: 30%;">

  <form method="POST" action="{{ route('artist.update', $artist->id) }}">
    {{ csrf_field() }}
    {{ method_field('PUT') }}
    
    <p><a href="{{ route('artist.index') }}">Go back</a></p>

    <div class="form-group">
      <label for="name">Name</label>
      <input type="text"
        name="name"
        id="name"
        class="form-control"
        value="{{ $artist->name }}"
        required
      />
    </div>
    
    <div class="form-group form-check">
      <label for="firstname">First Name</label>
      <input 
        type="text"
        name="firstname"
        id="firstname"
        class="form-control"
        value="{{ $artist->firstname}}" 
        required
      />
    </div>
    
    <div class="form-group form-check">
      <label for="birthdate">Birthdate</label>
      <input 
        type="text"
        name="birthdate"
        id="birthdate"
        class="form-control"
        value="{{ $artist->birthdate}}" 
        required
      />
    </div>
    
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</div>
@endsection

