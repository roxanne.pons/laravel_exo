@extends('layouts.app')

@section('content')
<div class="container mx-auto" style="width: 800px; margin-top:50px">
  
  <h1>List of actors</h1>
  <h4 style="padding-bottom:30px"><a href ="{{ route('artist.create')}}">Create new actor</a></h4>
  <table class="table table-stpied table-centered">
    <thead>
      <tr>
        <th>{{__('Name')}}</th>
        <th>{{__('First Name')}}</th>
        <th>{{__('Birthdate')}}</th>
        <th>{{__('Actions')}}</th>
      </tr>
    </thead>
    <tbody>
      @foreach($artists->sortBy('name') as $artist)
      <tr>
        <td>{{ $artist->name }}</td>
        <td>{{ $artist->firstname }}</td>
        <td>{{ $artist->birthdate }}</td>
        <td>
          <a type="button" 
            href="{{ route('artist.edit', $artist->id )}}" 
            class="btn"
            data-toggle="tooltip"
            title="@lang('Edit artist') {{ $artist->name }}">
            Edit <i class="fas fa-edit"></i>
          </a>
          <a type="button" 
            href="{{ route('artist.destroy', $artist->id )}}" 
            class="btn-delete"
            title="@lang('Delete artist') {{ $artist->name }}">
            Delete <i class="fas fa-trash-alt"></i>
          </a>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>

</div>

<!--pagination pour changer de page-->
{{ $artists->appends(request()->except('page'))->links() }}
</div>
  <!--securité + onclick btn delete-->
<script>
  $.ajaxSetup({
    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
  })

  $(document).on('click', '.btn-delete', function(){

    let button = $(this);

    $.ajax ({
      url: $(this).attr('href'),
      type: 'DELETE'
    }).done(function(){
      $(button).closest('tr').remove();
    });
    return false;
  });
</script>

@endsection