@extends('layouts.app')

@section('title', 'Create artist')

@section('content')

<div class="container" style="margin-top: 10em; width: 30%;">
  <p><a href="{{ route('artist.index') }}">Go back</a></p>
<h1>Create new actor</h1>

<form method="POST" action="{{ route('artist.store') }}">
  {{ csrf_field() }}
  
  <div class="form-group">
      <label for="name">Name</label>
      <input type="text"
        name="name"
        id="name"
        class="form-control"
        value=""
        required
      />
    </div>
    
    <div class="form-group form-check">
      <label for="firstname">First Name</label>
      <input 
        type="text"
        name="firstname"
        id="firstname"
        class="form-control"
        value="" 
        required
      />
    </div>
    
    <div class="form-group form-check">
      <label for="birthdate">Birthdate</label>
      <input 
        type="text"
        name="birthdate"
        id="birthdate"
        class="form-control"
        value="" 
        required
      />
    </div>
    
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</div>

@endsection