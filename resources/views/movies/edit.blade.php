@extends('layouts.app')

@section('title', 'Movie Title')

@section('content')
<div class="container" style="margin-top: 10em; width: 30%;">
  
  <form method="POST" action="{{ route('movie.update', $movie->id) }}" enctype="multipart/form-data">
    {{ csrf_field() }}
    {{ method_field('PUT') }}
    
    <p><a href="{{ route('movie.index') }}">Go back</a></p>


    <div class="form-group">
      <label for="title">Title</label>
      <input type="text"
        name="title"
        id="title"
        class="form-control"
        value="{{ $movie->title }}"
        required
      />
    </div>

    <div class="form-group form-check">
      <label for="year">Year</label>
      <input 
        type="text"
        name="year"
        id="year"
        class="form-control"
        value="{{ $movie->year}}" 
        required
      />
    </div>
    <div class="form-group">
      <label for="poster">Select a poster</label>
      <input type="file" class="form-control-file" id="poster">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</div>
@endsection
