@extends('layouts.app')

@section('title', 'Create Movie')

@section('content')

<div class="container" style="margin-top: 10em; width: 30%;">
  <p><a href="{{ route('movie.index') }}">Go back</a></p>
  <h1>Create new movie</h1>

  <form method="POST" action="{{ route('movie.store') }}" enctype="multipart/form-data">
    {{ csrf_field() }}
  
    <div class="form-group">
      <label for="title">Title</label>
      <input type="text"
        name="title"
        id="title"
        class="form-control"
        value=""
        required
      />
    </div>

    <div class="form-group form-check">
      <label for="year">Year</label>
      <input 
        type="text"
        name="year"
        id="year"
        class="form-control"
        value="" 
        required
      />
    </div>
    <div class="form-group">
      <label for="poster">Select an realisator</label>
    <select name="artist_id" class="form-control">
      @foreach($artists as $artist)
        <option value="{{ $artist->id }}">{{ $artist->name }} {{ $artist->firstname }}</option>
      @endforeach
    </select>
  </div>


    <div class="form-group">
      <label for="poster">Select a poster</label>
      <input type="file" class="form-control-file" id="poster">
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</div>
@endsection