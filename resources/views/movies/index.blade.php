@extends('layouts.app')

@section('content')
<div class="container mx-auto" style="width: 800px; margin-top:50px">
  <h1>List of movies</h1>
  <h4 style="padding-bottom:30px"><a href ="{{ route('movie.create')}}">Create new movie</a></h4>
 
  <table class="table table-stpied table-centered">
    <thead>
      <tr>
        <th>{{__('Title')}}</th>
        <th>{{__('Year')}}</th>
        <th>{{__('Poster')}}</th>
        <th>{{__('Actions')}}</th>
      </tr>
    </thead>
    <tbody>
      @foreach($movies as $movie)
      <tr>
        <td>{{ $movie->title }}</td>
        <td>{{ $movie->year }}</td>
        <td><img src="./uploads/posters/poster_{{ $movie->id }}.png" alt=""></td>
        <td>
          <a type="button" 
            href="{{ route('movie.edit', $movie->id )}}" 
            class="btn"
            data-toggle="tooltip"
            title="@lang('Edit movie') {{ $movie->title }}">
            Edit <i class="fas fa-edit"></i>
          </a>
          <a type="button" 
            href="{{ route('movie.destroy', $movie->id )}}" 
            class="btn-delete"
            title="@lang('Delete movie') {{ $movie->title }}">
            Delete <i class="fas fa-trash-alt"></i>
          </a>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  
</div>

<!--pagination pour changer de page-->
{{ $movies->appends(request()->except('page'))->links() }}
</div>
  <!--securité + onclick btn delete-->
<script>
  $.ajaxSetup({
    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
  })

  $(document).on('click', '.btn-delete', function(){

    let button = $(this);

    $.ajax ({
      url: $(this).attr('href'),
      type: 'DELETE'
    }).done(function(){
      $(button).closest('tr').remove();
    });
    return false;
  });
</script>

@endsection