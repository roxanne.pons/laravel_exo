@extends('layouts.app')

@section('title', 'Create Cinema')

@section('content')

<div class="container" style="margin-top: 10em; width: 30%;">
  <p><a href="{{ route('cinema.index') }}">Go back</a></p>
  <h1>Create new cinema</h1>

  <form method="POST" action="{{ route('cinema.store') }}">
    {{ csrf_field() }}
    
    <div class="form-group">
      <label for="name">Name</label>
      <input type="text"
        name="name"
        id="name"
        class="form-control"
        required
      />
    </div>

    <div class="form-group form-check">
      <label for="street">Street</label>
      <input 
        type="text"
        name="street"
        id="street"
        class="form-control"
        required
      />
    </div>

    <div class="form-group form-check">
      <label for="postcode">Postcode</label>
      <input 
        type="text"
        name="postcode"
        id="postcode"
        class="form-control"
        required
      />
    </div>

    <div class="form-group form-check">
      <label for="city">City</label>
      <input 
        type="text"
        name="city"
        id="city"
        class="form-control"
        required
      />
    </div>

    <div class="form-group form-check">
      <label for="country">Country</label>
      <input 
        type="text"
        name="country"
        id="country"
        class="form-control"
        required
      />
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</div>
@endsection