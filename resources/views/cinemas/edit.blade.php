@extends('layouts.app')

@section('title', 'Cinema Name')

@section('content')

<div class="container" style="margin-top: 10em; width: 30%;">

  <form method="POST" action="{{ route('cinema.update', $cinema->id) }}">
    {{ csrf_field() }}
    {{ method_field('PUT') }}
    <p><a href="{{ route('cinema.index') }}">Go back</a></p>

    <div class="form-group">
      <label for="name">Name</label>
      <input type="text"
        name="name"
        id="name"
        class="form-control"
        value="{{ $cinema->name}}" 
        required
      />
    </div>

    <div class="form-group form-check">
      <label for="street">Street</label>
      <input 
        type="text"
        name="street"
        id="street"
        class="form-control"
        value="{{ $cinema->street}}" 
        required
      />
    </div>

    <div class="form-group form-check">
      <label for="postcode">Postcode</label>
      <input 
        type="text"
        name="postcode"
        id="postcode"
        class="form-control"
        value="{{ $cinema->postcode}}" 
        required
      />
    </div>

    <div class="form-group form-check">
      <label for="city">City</label>
      <input 
        type="text"
        name="city"
        id="city"
        class="form-control"
        value="{{ $cinema->city}}" 
        required
      />
    </div>

    <div class="form-group form-check">
      <label for="country">Country</label>
      <input 
        type="text"
        name="country"
        id="country"
        class="form-control"
        value="{{ $cinema->country}}" 
        required
      />
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</div>
@endsection


