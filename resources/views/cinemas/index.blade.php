@extends('layouts.app')

@section('content')
<div class="container mx-auto" style="width: 800px; margin-top:50px">
  <h1>List of cinemas</h1>
  <h4 style="padding-bottom:30px"><a href ="{{ route('cinema.create')}}">Create new cinema</a></h4>
  <table class="table table-stpied table-centered">
    <thead>
      <tr>
        <th>{{__('Name')}}</th>
        <th>{{__('Street')}}</th>
        <th>{{__('Postcode')}}</th>
        <th>{{__('City')}}</th>
        <th>{{__('Country')}}</th>
      </tr>
    </thead>
    <tbody>
      @foreach($cinemas as $cinema)
      <tr>
        <td>{{ $cinema->name }}</td>
        <td>{{ $cinema->street }}</td>
        <td>{{ $cinema->postcode }}</td>
        <td>{{ $cinema->city }}</td>
        <td>{{ $cinema->country }}</td>
        <td>
          <a type="button" 
            href="{{ route('cinema.edit', $cinema->id )}}" 
            class="btn"
            data-toggle="tooltip"
            title="@lang('Edit cinema') {{ $cinema->name }}">
            Edit <i class="fas fa-edit"></i>
          </a>
          <a type="button" 
            href="{{ route('cinema.destroy', $cinema->id )}}" 
            class="btn-delete"
            title="@lang('Delete cinema') {{ $cinema->name }}">
            Delete <i class="fas fa-trash-alt"></i>
          </a>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>

</div>

<!--pagination pour changer de page-->
{{ $cinemas->appends(request()->except('page'))->links() }}
</div>
  <!--securité + onclick btn delete-->
<script>
  $.ajaxSetup({
    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
  })

  $(document).on('click', '.btn-delete', function(){

    let button = $(this);

    $.ajax ({
      url: $(this).attr('href'),
      type: 'DELETE'
    }).done(function(){
      $(button).closest('tr').remove();
    });
    return false;
  });
</script>

@endsection