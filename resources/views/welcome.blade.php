@extends('layouts.app')

@section('title', 'Cinema DB')

@section('content')
<div class="container mx-auto" style="width: 700px; margin-top:50px">
  <h1 class="text-center" style="padding-bottom:20px">CINEMA DATA BASE</h1>
  <a href="{{ route('artist.index') }}" type="button" class="btn btn-dark btn-lg">View artists list</a>
  <a href="{{ route('movie.index') }}" type="button" class="btn btn-dark btn-lg">View movies list</a>
  <a href="{{ route('cinema.index') }}" type="button" class="btn btn-dark btn-lg">View theaters list</a>
  <a href="{{ route('room.index') }}" type="button" class="btn btn-dark btn-lg">View Rooms list</a>
</div>
@endsection