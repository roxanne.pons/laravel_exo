@extends('layouts.app')

@section('title', 'Create Room')

@section('content')

<div class="container" style="margin-top: 10em; width: 30%;">
  <p><a href="{{ route('room.index') }}">Go back</a></p>
<h1>Create new room</h1>

<form method="POST" action="{{ route('room.store') }}">
  {{ csrf_field() }}
 
  <div class="form-group">
    <label for="name">Name</label>
    <input type="text"
      name="name"
      id="name"
      class="form-control"
      value=""
      required
    />
  </div>

  <div class="form-group form-check">
    <label for="capacity">Capacity</label>
    <input 
      type="text"
      name="capacity"
      id="capacity"
      class="form-control"
      value="" 
      required
    />
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
</div>
@endsection