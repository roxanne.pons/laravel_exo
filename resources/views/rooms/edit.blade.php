@extends('layouts.app')

@section('title', 'Room Name')

@section('content')
<div class="container" style="margin-top: 10em; width: 30%;">

  <form method="POST" action="{{ route('room.update', $room->id) }}">
    {{ csrf_field() }}
    {{ method_field('PUT') }}
    <p><a href="{{ route('room.index') }}">Go back</a></p>

    <div class="form-group">
      <label for="name">Name</label>
      <input type="text"
        name="name"
        id="name"
        class="form-control"
        value="{{ $room->name }}"
        required
      />
    </div>

    <div class="form-group form-check">
      <label for="capacity">Capacity</label>
      <input 
        type="text"
        name="capacity"
        id="capacity"
        class="form-control"
        value="{{ $room->capacity}}" 
        required
      />
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</div>

@endsection