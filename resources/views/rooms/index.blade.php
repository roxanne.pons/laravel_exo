@extends('layouts.app')

@section('content')
<div class="container mx-auto" style="width: 800px; margin-top:50px">
  <h1>List of Rooms</h1>
  <h4 style="padding-bottom:30px"><a href ="{{ route('room.create')}}">Create new room</a></h4>
  <table class="table table-stpied table-centered">
    <thead>
      <tr>
        <th>{{__('Name')}}</th>
        <th>{{__('Capacity')}}</th>
      </tr>
    </thead>
    <tbody>
      @foreach($rooms as $room)
      <tr>
        <td>{{ $room->name }}</td>
        <td>{{ $room->capacity }}</td>
        <td>
          <a type="button" 
            href="{{ route('room.edit', $room->id )}}" 
            class="btn"
            data-toggle="tooltip"
            title="@lang('Edit room') {{ $room->name }}">
            Edit <i class="fas fa-edit"></i>
          </a>
          <a type="button" 
            href="{{ route('room.destroy', $room->id )}}" 
            class="btn-delete"
            title="@lang('Delete room') {{ $room->name }}">
            Delete <i class="fas fa-trash-alt"></i>
          </a>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>

</div>
<!--pagination pour changer de page-->
{{ $rooms->appends(request()->except('page'))->links() }}
</div>
  <!--securité + onclick btn delete-->
<script>
  $.ajaxSetup({
    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
  })

  $(document).on('click', '.btn-delete', function(){

    let button = $(this);

    $.ajax ({
      url: $(this).attr('href'),
      type: 'DELETE'
    }).done(function(){
      $(button).closest('tr').remove();
    });
    return false;
  });
</script>

@endsection