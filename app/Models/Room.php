<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $fillable = [
        'name', 'capacity'
    ];

    // public function belongsTo()
    // {
    //     return $this->belongsTo('App\Models\Room', 'room_id');
    // }
}
