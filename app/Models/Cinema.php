<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cinema extends Model
{
    protected $fillable = [
        'name', 'street', 'postcode', 'city', 'country'
    ];

    public function has_movies()
    {
        return $this->hasMany('App\Models\Movie');
    }

    public function has_rooms()
    {
        return $this->hasMany('App\Models\Room');
    }
}
